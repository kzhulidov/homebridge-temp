const { exec } = require('child_process');
var Service, Characteristic;

module.exports = function (homebridge) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  homebridge.registerAccessory("homebridge-temp", "TempSensor", TempSensor);
};

function TempSensor(log, config, api) {
  this.name = config["name"];
  this.cmd = config["cmd"];
}

TempSensor.prototype = {
  getServices: function () {
    let informationService = new Service.AccessoryInformation();
    informationService
      .setCharacteristic(Characteristic.Manufacturer, "Temp sensor manufacturer")
      .setCharacteristic(Characteristic.Model, "Temp sensor model")
      .setCharacteristic(Characteristic.SerialNumber, "123-456-789");

    let tempService = new Service.TemperatureSensor(this.name);
    tempService
      .getCharacteristic(Characteristic.CurrentTemperature)
      .on('get', (callback) => { 
        exec(this.cmd, (error, stdout, stderr) => callback(null, parseFloat(stdout)));
      });

    return [informationService, tempService];
  }
};

