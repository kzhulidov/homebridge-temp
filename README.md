# homebridge-temp

[Homebridge](https://homebridge.io) plugin of a temperature sensor accessory.

## Installation

```shell
sudo npm install -g homebridge-temp
```

## Configuration

Add this to the `accessories` list in *.homebridge/config.json*:

```json
{
  "accessory": "TempSensor",
  "name": "My sensor",
  "cmd": "<command>"
}
```

`<command>` is a shell command returning a string containing a single floating point value. This value will be returned as the sensor reading, in degrees C.
